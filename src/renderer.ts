// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.



import { BoxGeometry, Mesh, MeshBasicMaterial, PerspectiveCamera, Scene, WebGLRenderer } from 'THREE';


console.log("hello from ts...");
initApp();

function initApp() {
  console.log('App initialized!');

  /* stats */

  let scene: THREE.Scene, camera: THREE.PerspectiveCamera, renderer: THREE.WebGLRenderer;
  let geometry: THREE.Geometry, material: THREE.Material, mesh: THREE.Mesh;

  init();
  animate();
  function init() {

    scene = new Scene();

    camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 10000);
    camera.position.z = 1000;

    geometry = new BoxGeometry(200, 200, 200);
    material = new MeshBasicMaterial({ color: 0xff0000, wireframe: true });

    mesh = new Mesh(geometry, material);
    scene.add(mesh);

    renderer = new WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);

    document.body.appendChild(renderer.domElement);

  }

  function animate() {


    mesh.rotation.x += 0.01;
    mesh.rotation.y += 0.02;

    renderer.render(scene, camera);

    requestAnimationFrame(animate);
  }

}







